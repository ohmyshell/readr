const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'src/index.js'),
  target: 'electron-renderer',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/, /app.js/],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      }
    ]
  }
};
