const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const pdfparse = require('pdf-parse');
const fs = require('fs');

let win;
let isFullscreen = false;

function createWindow() {
  win = new BrowserWindow({
    minWidth: 400,
    minHeight: 400,
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      devTools: process.env.DEV_ENV ? true : false
    }
  });

  // and load the index.html of the app.
  if (process.env.DEV_ENV) {
    win.loadURL('http://127.0.0.1:8080/');
    win.webContents.openDevTools();
  } else win.loadFile('build/index.html');

  win.on('closed', () => {
    win = null;
  });
}

app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});

ipcMain.on('exit', () => {
  win.close();
  win = null;
});

ipcMain.on('minimize', () => {
  win.minimize();
});

ipcMain.on('fullscreen', () => {
  isFullscreen = !isFullscreen;
  win.setFullScreen(isFullscreen);
});

ipcMain.on('getPdfText', async e => {
  let result = await dialog.showOpenDialog(win, {
    filters: [
      {
        name: 'PDF Document',
        extensions: ['pdf']
      }
    ],
    properties: ['openFile']
  });
  if (result.canceled) return;
  let file = fs.readFileSync(result.filePaths[0]);
  let { text } = await pdfparse(file);
  e.reply('text', text);
});
