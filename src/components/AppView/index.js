import React from 'react';
import './AppView.css';
import { ThemeContext } from '../../contexts/ThemeContext';

export default function AppView(props) {
  const { theme } = React.useContext(ThemeContext);
  return (
    <div id="appview" className={theme.isDark ? 'dark' : 'light'}>
      {props.children}
    </div>
  );
}
