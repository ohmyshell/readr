import React from 'react';
import { ThemeContext } from '../../contexts/ThemeContext';
import './Titlebar.css';
const ipcRenderer = require('electron').ipcRenderer;

export default function TitleBar() {
  const { theme } = React.useContext(ThemeContext);

  const style = theme.dark;

  function getTaskbarOs() {
    switch (window.navigator.platform) {
      case 'Win32':
        return 'windows';
    }
  }

  function minimizeApp() {
    ipcRenderer.send('minimize');
  }

  function fullscreen() {
    ipcRenderer.send('fullscreen');
  }

  function exitApp() {
    ipcRenderer.send('exit');
  }

  return (
    <div
      id="titlebar"
      className={`${getTaskbarOs()} ${theme.isDark ? 'dark' : 'light'}`}
    >
      <section className="title">readr</section>
      <div>
        <button onClick={fullscreen}>{fullscreenButton}</button>
        <button onClick={minimizeApp}>{minimizeButton}</button>
        <button onClick={exitApp}>{exitButton}</button>
      </div>
    </div>
  );
}

const exitButton = (
  <svg
    width="12"
    height="12"
    viewBox="0 0 12 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0.659285 0.280286C1.05677 -0.103149 1.68983 -0.0917561 2.07327 0.305732L6 4.37638L9.92673 0.305732C10.3102 -0.0917561 10.9432 -0.103149 11.3407 0.280286C11.7382 0.66372 11.7496 1.29678 11.3662 1.69427L7.38944 5.81674L11.7197 10.3057C12.1032 10.7032 12.0918 11.3363 11.6943 11.7197C11.2968 12.1031 10.6637 12.0918 10.2803 11.6943L6 7.25711L1.71972 11.6943C1.33628 12.0918 0.703219 12.1031 0.305732 11.7197C-0.0917561 11.3363 -0.103149 10.7032 0.280286 10.3057L4.61056 5.81674L0.633839 1.69427C0.250405 1.29678 0.261797 0.66372 0.659285 0.280286Z"
      fill="black"
    />
  </svg>
);

const fullscreenButton = (
  <svg
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0.5 1C0.5 0.723858 0.723858 0.5 1 0.5H3.25C3.52614 0.5 3.75 0.723858 3.75 1C3.75 1.27614 3.52614 1.5 3.25 1.5H2.20711L7.53033 6.82322L6.82322 7.53033L1.5 2.20711V3.25C1.5 3.52614 1.27614 3.75 1 3.75C0.723858 3.75 0.5 3.52614 0.5 3.25V1ZM9.17678 8.46967L14.5 13.7929V12.75C14.5 12.4739 14.7239 12.25 15 12.25C15.2761 12.25 15.5 12.4739 15.5 12.75V15C15.5 15.2761 15.2761 15.5 15 15.5H12.75C12.4739 15.5 12.25 15.2761 12.25 15C12.25 14.7239 12.4739 14.5 12.75 14.5H13.7929L8.46967 9.17678L9.17678 8.46967Z"
      fill="black"
    />
  </svg>
);

const minimizeButton = (
  <svg
    width="12"
    height="2"
    viewBox="0 0 12 2"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0 1C0 0.447715 0.447715 0 1 0L11 0C11.5523 0 12 0.447715 12 1C12 1.55228 11.5523 2 11 2L1 2C0.447715 2 0 1.55228 0 1Z"
      fill="black"
    />
  </svg>
);
