import React from 'react';
import ReactDom from 'react-dom';
import TitleBar from './components/Titlebar';
import Sidebar from './components/Sidebar';
import Book from './components/Book';
import AppView from './components/AppView';
import { ThemeContextProvider } from './contexts/ThemeContext';
import './global.css';
const ipcRenderer = require('electron').ipcRenderer;

function App() {
  const [text, setText] = React.useState('');
  ipcRenderer.on('text', (_, arg) => {
    setText(arg);
    console.log(text);
  });
  function openBook() {
    ipcRenderer.send('getPdfText');
  }
  return (
    <React.Fragment>
      <ThemeContextProvider>
        <TitleBar />
        <AppView>
          <Sidebar openBook={openBook} />
          <Book text={text} />
        </AppView>
      </ThemeContextProvider>
    </React.Fragment>
  );
}

ReactDom.render(<App />, document.getElementById('app'));
